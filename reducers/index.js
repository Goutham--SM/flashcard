import {
  RECEIVEDECKS,
  ADD_D,
  ADD_C,
  REM_D,
  REM_C,
} from "../actions";

const decks = (state = {}, action) => {
  switch (action.type) {
    case RECEIVEDECKS:
      return {
        ...state,
        ...action.alldecks
      };
    case ADD_D:
      const { deck } = action;
      return {
        ...state,
        [deck]: {
          title: deck,
          questions: []
        }
      };
    case REM_D:
      const newState = Object.assign({}, state)
      delete newState[action.id];
      return newState;
    case ADD_C:
      const { title, card } = action;
      return {
        ...state,
        [title]: {
          ...state[title],
          questions: state[title].questions.concat(card)
        }
      };
    case REM_C:
      const { deckQuestion } = action;
      const { question: qst, name: nm } = deckQuestion;
      return {
        ...state,
        [nm]: {
          ...state[nm],
          questions: state[nm].questions.filter(q => q.question !== qst)
        }
      };
    default:
      return state;
  }
};

export default decks;
