import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

class DeckView extends React.Component{

  render(){
    if(this.props.selectedDeck!==undefined){
    return (
      <View style={styles.deckContainer}>
      <Text style={styles.deckText}>{this.props.selectedDeck.title}</Text>
      <Text style={styles.cardText}>It has {this.props.selectedDeck.questions.length} cards</Text>
      </View>
    );
  }else{
      return (<View><Text>Removing...</Text></View>);
    }
  }
}

const styles = StyleSheet.create({
  deckContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexBasis: 120,
    minHeight: 120,
    borderWidth: 1,
    borderColor: '#2c3531',
    backgroundColor: '#116466',
    borderRadius: 5,
    marginBottom: 10
  },
  deckText: {
    fontSize: 28,
    color:"#ffcb9a"
  },
  cardText: {
    fontSize: 18,
    color: "#ffcb9a"
  }
});

const mapStateToProps = (state, { id }) => {
  const selectedDeck = state[id];

  return {
    selectedDeck
  };
};
export default connect(mapStateToProps)(DeckView);
