import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import { connect } from 'react-redux';
import { getInitialData } from '../actions';
import {getDecks} from '../utils/AsyncStoreAndDataAPI';

import DeckView from './DeckView';
export class Homepage extends React.Component {
  componentDidMount(){
    this.props.getInitialData();
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.block}>
          <Text style={styles.title}>All Decks</Text>
          {Object.values(this.props.decks).map(D =>{
            return(
              <TouchableOpacity
              key={D.title}
              onPress={() =>
                this.props.navigation.navigate('Detail', { title: D.title })}>
              <DeckView id={D.title} />
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    backgroundColor: "#d1e8e2"
  },
  title: {
    fontSize: 40,
    textAlign: 'center',
    marginBottom: 16,
    color: '#116466'
  }
});
const mapStateToProps = state => ({ decks: state });
export default connect(
  mapStateToProps,
  { getInitialData }
)(Homepage);
