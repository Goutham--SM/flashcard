import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Homepage from './Homepage';
import AddDecks from './AddDecks';
import AddQuestions from './AddQuestions';
import DeckContent from './DeckContent';
import Quiz from './Quiz';

const Stack = createStackNavigator();

const Tab = createMaterialTopTabNavigator();

function Tabs() {
  return (
      <Tab.Navigator>
        <Tab.Screen name="Home" component={Homepage} />
        <Tab.Screen name="AddDeck" component={AddDecks} />
      </Tab.Navigator>
  );
}


export default function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Tabs} />
      <Stack.Screen name ="Detail" component={DeckContent}/>
      <Stack.Screen name="AddCard" component={AddQuestions}/>
      <Stack.Screen name="Quiz" component={Quiz}/>

    </Stack.Navigator>
  );
}
