import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {handleRemoveDeck} from '../actions'
import {deleteDeck} from '../utils/AsyncStoreAndDataAPI';
import DeckView from './DeckView';
class DeckContent extends React.Component{
  removeDeck =(id)=>{
    deleteDeck(id);
    this.props.handleRemoveDeck(id);
    this.props.navigation.navigate('Home');
  }
  render(){
    const {selectedDeck} = this.props;
    return (
      <View style={styles.container}>
        <DeckView id={this.props.title}/>
        <TouchableOpacity onPress = {() =>this.props.navigation.navigate('Quiz', { title: this.props.title })}
          style={styles.btn}>
          <Text style={styles.btnText}>Start Quiz</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() =>this.props.navigation.navigate('AddCard', { title: this.props.title })}
        style={styles.btn}>
          <Text style={styles.btnText}>Add Card</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() =>this.removeDeck(this.props.title)}
        style={styles.btn}>
          <Text style={styles.btnText}>Remove Deck</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btn:{
    backgroundColor: "#2c3531",
    includeFontPadding:true,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#2c3531',
    marginLeft:32,
    margin:32,
    marginRight:32
  },
  btnText:{
    borderRadius:10,
    borderWidth: 1,
    backgroundColor:'#ffcb9a',
    textAlign:'center',
    padding:12
  },
  container: {
    flex: 1,
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    backgroundColor: "#d1e8e2"
  }
});

const mapStateToProps = (state, {route}) => {
  const {title} = route.params;
  const selectedDeck = state[title];

  return {
    title,
    selectedDeck
  };
};
export default connect(mapStateToProps,{handleRemoveDeck})(DeckContent);
