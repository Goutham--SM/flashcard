import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MaterialIcons, Octicons } from '@expo/vector-icons'
import { createStore } from "redux";
import AllNavigation from './AllNavigation';
import { Provider } from "react-redux";
import { NavigationContainer } from '@react-navigation/native';
import reducer from "../reducers/";
import { setLocalNotification } from '../utils/helper';

import middleware from "../middleware";
const store = createStore(reducer, middleware);
// Tabs = createMaterialTopTabNavigator({
//   AddDecks:{
//     screen:AddDecks,
//     navigationOptions:{
//       tabBarLabel:'Add Decks'
//     }
//   },
//   AddQuestions:{
//     screen:AddQuestions,
//     navigationOptions:{
//       tabBarLabel:'Add Questions'
//     }
//   }
//   },
//   {
//     tabBarOptions:{
//       activeTintColor: '#000',
//       style:{
//         height:60,
//         backgroundColor:'#fff'
//       }
//     }
//   }
// );

export default class App extends React.Component{
  componentDidMount() {
        setLocalNotification();
    }
  render(){
  return (
  <Provider store={store}>
    <NavigationContainer>
      <AllNavigation/>
    </NavigationContainer>
    </Provider>
  );
}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
