import React from 'react';
import {addCard} from '../utils/AsyncStoreAndDataAPI';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { handleAddCard, getInitialData } from '../actions';

class AddQuestions extends React.Component {
  state = {
    question: '',
    answer:'',
  };
  handleQuesChange = (question) => {
    this.setState({question});
  };
  handleAnsChange = (answer) => {
    this.setState({answer});
  };
  handleSubmit = () => {
    const c = {
      question: this.state.question,
      answer: this.state.answer
    };
    addCard(this.props.title,c);
    this.props.handleAddCard(this.props.title,c);
    this.setState(() => (
      {
        question: '',
        answer:''
      }
    ));
    this.props.navigation.navigate('Home');
  };
  render() {
    console.log(this.props.title);
    return (
      <View style={styles.container}>
      <View style={styles.block}>
        <Text style={styles.title}>Adding question to {this.props.title}</Text>
      </View>
        <View style={styles.block}>
          <Text style={styles.text}>Enter the question</Text>
        </View>
        <View style={[styles.block]}>
          <TextInput
            id='question'
            style={styles.input}
            value={this.state.question}
            onChangeText={this.handleQuesChange}
          />
        </View>
        <View style={styles.block}>
          <Text style={styles.text}>Enter the answer</Text>
        </View>
        <View style={[styles.block]}>
          <TextInput
            id='answer'
            style={styles.input}
            value={this.state.answer}
            onChangeText={this.handleAnsChange}
          />
        </View>
          <TouchableOpacity disabled={this.state.question === '' || this.state.answer === ''} onPress={()=> this.handleSubmit()} style={styles.btn}>
            <Text style={styles.btnText}>Submit</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#d1e8e2",
    alignItems:'center',
    justifyContent:'center'
  },
  text:{
    color:'#116466',
    textAlign:'center',
    fontSize: 28,
  },
  btn:{
    backgroundColor: "#2c3531",
    includeFontPadding:true,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#2c3531',
    marginLeft:32,
    margin:32,
    marginRight:32
  },
  btnText:{
    borderRadius:10,
    borderWidth: 1,
    backgroundColor:'#ffcb9a',
    textAlign:'center',
    padding:12
  },
  block: {
    marginBottom: 20
  },
  title: {
    textAlign: 'center',
    fontSize: 32,
    color:'#116466',
    marginBottom:20
  },
  input: {
    borderWidth: 1,
    borderColor: '#000',
    backgroundColor: '#fff',
    width:240,
    borderRadius: 5,
    fontSize: 20,
    height: 40,
    marginBottom: 20
  }
});
const mapStateToProps = (state, {route}) => {
  const {title} = route.params;
  return {
    title,
  };
};
export default connect(mapStateToProps,{ handleAddCard, getInitialData })(AddQuestions);
