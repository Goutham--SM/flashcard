import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Pages } from 'react-native-pages';
import { setLocalNotification, clearLocalNotification } from '../utils/helper';
import QuestionView from  './QuestionView';
class Quiz extends React.Component{
  componentDidMount() {
        clearLocalNotification()
        setLocalNotification()
    }
  state={
    numQues: this.props.currentDec.questions.length,
    correct:0,
    incorrect:0,
    answeredQues:0
  }
  handleReset=()=>{
    this.setState(()=>({
      correct:0,
      incorrect:0,
      completed:false
    }));
    this.props.navigation.navigate('Home');
  }
  markQuestion=(k)=>{
    this.setState(ps=>({answeredQues:++ps.answeredQues}));
    k==='correct'?this.setState(ps=>({correct: ++ps.correct})):this.setState(ps=>({incorrect: ++ps.incorrect}));
  }
  render(){
    console.log(this.state.incorrect);

    if(this.state.numQues===0){
      return(
        <View style={styles.container}>
          <Text style={styles.noCards}>Please add cards to start the quiz</Text>
        </View>
      );
    }else if(!(this.state.answeredQues===this.state.numQues)){
    console.log(this.props.currentDec);
    return(
      <Pages startPage={0} indicatorPosition='bottom'>
          {Object.values(this.props.currentDec.questions).map(D =>{
            return(
              <View key={D.question} style={styles.container}>
              <QuestionView id={D} markQuestion={this.markQuestion}/>
              </View>
            );
          })}
        </Pages>
    );
  }else{
    return(
    <View style={styles.container}>
      <Text style={styles.deckText}>{this.props.currentDec.title} is Complete!</Text>
      <Text style={styles.deckText}>Score is {this.state.correct} out of {this.state.numQues}</Text>
      <TouchableOpacity onPress={()=> this.props.navigation.navigate('Home')} style={styles.btn}>
        <Text style={styles.btnText}>Go Back</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={this.handleReset} style={styles.btn}>
        <Text style={styles.btnText}>Reset</Text>
      </TouchableOpacity>
    </View>
  );
  }
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#d1e8e2",
    alignItems:'center',
    justifyContent:'center'
  },
  btn:{
    backgroundColor: "#2c3531",
    includeFontPadding:true,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#2c3531',
    marginLeft:32,
    margin:32,
    marginRight:32
  },
  btnText:{
    borderRadius:10,
    borderWidth: 1,
    backgroundColor:'#ffcb9a',
    textAlign:'center',
    padding:12
  },
  noCards:{
    color:'#116466',
    fontSize: 28,
    textAlign:'center'
  },
  deckText: {
    fontSize: 28,
    color:"#116466"
  }
});
const mapStateToProps=(state,{route}) => {
  const {title} = route.params;
  const currentDec = state[title];
  return{
    title,
    currentDec
  };
}
export default connect(mapStateToProps)(Quiz);
