import React from 'react';
import { createAppContainer } from 'react-navigation';
import AllNavigation from './AllNavigation';

export default createAppContainer(AllNavigation);
