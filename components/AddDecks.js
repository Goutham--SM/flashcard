import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { handleAddDeck, getInitialData } from '../actions';
import { saveDeck } from '../utils/AsyncStoreAndDataAPI';

export class AddDeck extends React.Component {
  state = {
    text: ''
  };
  handleChange = text => {
    this.setState({ text });
  };
  handleSubmit = () => {
    const { handleAddDeck, navigation } = this.props;
    const { text } = this.state;

    handleAddDeck(text);
    saveDeck(text);
    this.props.getInitialData();
    this.setState(() => ({ text: '' }));
    navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.block}>
          <Text style={styles.title}>Enter the name of the Deck?</Text>
        </View>
        <View style={[styles.block]}>
          <TextInput
            style={styles.input}
            value={this.state.text}
            onChangeText={this.handleChange}
          />
        </View>
          <TouchableOpacity disabled={this.state.text === ''} onPress={this.handleSubmit} style={styles.btn}>
            <Text style={styles.btnText}>Submit</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#d1e8e2",
    alignItems:'center',
    justifyContent:'center'
  },
  btn:{
    backgroundColor: "#2c3531",
    includeFontPadding:true,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#2c3531',
    marginLeft:32,
    margin:32,
    marginRight:32
  },
  btnText:{
    borderRadius:10,
    borderWidth: 1,
    backgroundColor:'#ffcb9a',
    textAlign:'center',
    padding:12
  },
  block: {
    marginBottom: 20
  },
  title: {
    textAlign: 'center',
    fontSize: 32,
    color:'#116466'
  },
  input: {
    borderWidth: 1,
    borderColor: '#000',
    backgroundColor: '#fff',
    width:240,
    borderRadius: 5,
    fontSize: 20,
    height: 40,
    marginBottom: 20
  }
});

export default connect(
  null,
  { handleAddDeck, getInitialData }
)(AddDeck);
