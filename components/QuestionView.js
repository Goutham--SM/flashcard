import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

class QuestionView extends React.Component{
  state={
    answered: false,
    viewAns:false
  }
  handleSubmitCorrect = () =>{
    this.setState({answered:true});
    this.props.markQuestion('correct');
  }
  handleSubmitIncorrect = () =>{
    this.setState({answered:true});
    this.props.markQuestion('incorrect');
  }
  handleAns = e=>{
    this.setState(ps=>({viewAns:!ps.viewAns}));
  }
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.deckContainer}>
        {!this.state.viewAns?
          <Text style={styles.deckText}>{this.props.id.question}</Text>:
          <Text style={styles.cardText}>{this.props.id.answer}</Text>}
        </View>
        <TouchableOpacity disabled={this.state.answered===true} onPress={this.handleSubmitCorrect} style={styles.btn}>
          <Text style={styles.btnText}>Correct</Text>
        </TouchableOpacity>
        <TouchableOpacity disabled={this.state.answered===true} onPress={this.handleSubmitIncorrect} style={styles.btn}>
          <Text style={styles.btnText}>Incorrect</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.handleAns} style={styles.btn}>
        {!this.state.viewAns?
          <Text style={styles.btnText}>View answer</Text>:
          <Text style={styles.btnText}>Hide answer</Text>
        }
        </TouchableOpacity>
        {this.state.answered?<Text>Answered</Text>:<Text>Unanswered</Text>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"#d1e8e2",
  
  },
  btn:{
    backgroundColor: "#2c3531",
    includeFontPadding:true,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#2c3531',
    marginLeft:32,
    margin:32,
    marginRight:32
  },
  btnText:{
    borderRadius:10,
    borderWidth: 1,
    backgroundColor:'#ffcb9a',
    textAlign:'center',
    padding:12
  },
  deckContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexBasis: 120,
    minHeight: 120,
    borderWidth: 1,
    borderColor: '#2c3531',
    backgroundColor: '#116466',
    borderRadius: 5,
    marginBottom: 10
  },
  deckText: {
    fontSize: 18,
    color:"#ffcb9a"
  },
  cardText: {
    fontSize: 18,
    color: "#d1e8e2"
  }
});

const mapStateToProps = (state, { id }) => {
  return{
    id
  };
};
export default connect(mapStateToProps)(QuestionView);
