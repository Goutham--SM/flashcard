let decks ={
  React: {
    title: 'React',
    questions: [
      {
        question: 'Is React a library for managing user interfaces?',
        answer: 'Correct'
      },
      {
        question: 'Does componentDidMount lifecycle event make Ajax requests in React?',
        answer: 'Correct'
      }
    ]
  },
  JavaScript: {
    title: 'JavaScript',
    questions: [
      {
        question: 'Can the combination of a function and the lexical environment within which that function was declared called as closure?',
        answer: 'Correct.'
      }
    ]
  }
};

//Initial computation of functions completely rough
// export function _getDecks(){
//   return new Promise((res, rej) => {
//     setTimeout(() => res({ ...decks }), 500);
//   });
// }
//
// export function _getDeck(id){
//   return new Promise((res,rej) => {
//     setTimeout(()=> res({decks[id]}),500);
//   });
// }
//
// export function _saveTitle(title){
//   return new Promise((res,rej)=> {
//     setTimeout(()=> {
//       decks = {
//         ...decks,
//         [title]:{
//           title:title,
//           questions:[]
//         }
//       };
//     });
//     res(decks[title]);
//   },500);
// }
//
// export function _addCardToDeck(id,ques){
//   return new Promise((resolve, reject)=> {
//     setTimeout(()=> {
//       decks = {
//         ...decks,
//         [id]:{
//           ...decks[id],
//           questions: decks[deck].questions.concat([ques])
//         }
//       };
//       resolve(decks[id])
//     }, 500);
//   });
// }
//
// export function _deleteDeck(id){
//   return new Promise((resolve, reject)=> {
//     setTimeout(()=>{
//       decks[id]={};
//       delete decks[id];
//       resolve(decks);
//     }, 500);
//   });
// }
//
// export function _deleteCard(id,ques){
//   return new Promise((resolve, reject)=> {
//     setTimeout(()=>{
//       decks={
//         ...decks,
//         [id]:{
//           ...decks[id],
//           questions:decks[id].questions.filter((q)=> q.question!== ques)
//         }
//       };
//       resolve(decks[id]);
//     }, 500);
//   });
// }
