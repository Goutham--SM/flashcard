import React from 'react';
import { AsyncStorage } from "react-native";
import {decks} from './_DATA';
export const DECKS_KEY = "flashcards:quiz";


export function getData() {
  return decks;
}

function formatDeckResults(results) {
  return results === null ? decks : JSON.parse(results);
}

export const getDecks = async() =>{
  try{
    const storeResults = await AsyncStorage.getItem(DECKS_KEY);

    if (storeResults === null) {
      AsyncStorage.setItem(DECKS_KEY, JSON.stringify(decks));
    }

    return storeResults === null ? decks : JSON.parse(storeResults);
  } catch (error) {
    console.log('The Error is:', error);
  }
}


export const getDeckByTitle = async(title) =>{
  return AsyncStorage.getItem(DECKS_KEY).then(results => results[title]);
}


export function saveDeck(title) {
  return AsyncStorage.mergeItem(
    DECKS_KEY,
    JSON.stringify({
      [title]: {
        title: title,
        questions: []
      }
    })
  );
}

export async function deleteDeck(deckId) {
    const results = await AsyncStorage.getItem(DECKS_KEY);
    if (results) {
        const data = JSON.parse(results);
        delete data[deckId];
        await AsyncStorage.setItem(DECKS_KEY, JSON.stringify(data))
        return data;
    }
    return {};
}

export function addCard(title,c) {
  return AsyncStorage.getItem(DECKS_KEY).then(results => {
    let decks = { ...JSON.parse(results) };
    decks = {
      ...decks,
      [title]: {
        ...decks[title],
        questions: decks[title].questions.concat(c)
      }
    };
    AsyncStorage.mergeItem(DECKS_KEY, JSON.stringify(decks));
  });
}
