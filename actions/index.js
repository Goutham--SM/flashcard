export const RECEIVEDECKS = 'RECEIVE_DECKS';
export const ADD_D = 'ADD_DECK';
export const ADD_C = ' ADD_CARD';
export const REM_D = 'REMOVE_DECK';
export const REM_C = 'REMOVE_CARD';
export const RECEIVE_SINGLE_DECK = 'RECEIVE_SINGLE_DECK';
import {getDecks, getDeckByTitle, saveDeck, deleteDeck, addCard} from '../utils/AsyncStoreAndDataAPI'
export function addDeck(deck){
  return {
    type: ADD_D,
    deck
  };
}

export function addCards(title,card){
  return {
    type:ADD_C,
    title,
    card
  };
}

export function removeCard(deckQuestion){
  return {
    type:REM_C,
    deckQuestion
  };
}

export function removeDeck(deckTitle){
  return {
    type:REM_D,
    deckTitle
  };
}

export function receiveDecks(alldecks){
  return {
    type:RECEIVEDECKS,
    alldecks
  };
}

export function receiveSingleDeck(deck){
  return {
    type:RECEIVE_SINGLE_DECK,
    deck
  };
}

export function getInitialData(){
  return (dispatch)=>{
    return getDecks().then(data => dispatch(receiveDecks(data)));
  };
}

export function handleAddDeck(title){
  return (dispatch)=>{
    return saveDeck(title).then(()=> addDeck(title));
  };
}

export function handleAddCard(title, {ques, ans}){
  return (dispatch) =>{
    return addCard(title,{ques,ans}).then(()=> dispatch(addCards(title,{ques,ans})));
  };
}

export function handleSingleDeck(title){
  return (dispatch) =>{
    return getDeckByTitle().then(()=> dispatch(receiveSingleDeck(title)));
  };
}

export function handleRemoveDeck(id){
  return (dispatch)=>{
    return deleteDeck(id).then(()=>dispatch(removeDeck(id)));
  }
}
