#Mobile Flash Cards App

##Installation
###1. To install the application enter yarn install to install all the dependencies that are required to run this app.
###2. Once the installation is done go ahead and download the EXPO app that is available in the app store or google play store.
###3. After installing the EXPO, navigate to the downloaded folder and start the terminal and type expo start to start the app through EXPO.
###4. After JS bundle are produced, a qr code along with few commands are displayed. IF physical device is used open the EXPO app and scan the qr code. If a simulator is used, press a from the terminal or choose the option run through the Simulator or connected devices options that is enabled on the local host webpage.

## This app is created to refresh the memory of a person who is planning to learn something new. This allows the users to create new decks which contains all the related questions/cards within that particular deck. After creating the decks and cards then the quiz can be began by the users.

#Flow of the application
##1. The app is made up of 2 navigation view, a tab navigator and a stack navigator.
##2. By default all the decks that are present within the local AsynStore is displayed along with the tab view which allows to navigate to the home view or AddDecks View
##3. The AddDecks view allows the user to add the decks to the AsynStore thus rendering it to the app.
##4. From the home view, if a particular deck is clicked, a detailed view is displayed which allows the user to
  ###i. To start the quiz: start answering the test based on the existing set of Cards
  ####a. Each deck displays the questions in a form of page which allows the user to note their guesses 'correct or incorrect'. Each page also allows the user to view the answer for the question.
  ####b. Once all the cards are answered the results are displayed and the user is allowed to either go back to the home page or reset the quiz so that they can take them from the beginning.
  ###ii. There is an option to remove the deck as well which gives a the user the ability to remove the deck entirely.
  ###iii. There is an other option AddCard which allows the user to add questions and answer to a particular deck for future quizzes.


#NOTE: Tested with andorid simulator and android mobile (Mi Max 2 - physical device)
